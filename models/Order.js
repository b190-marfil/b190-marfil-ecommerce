const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	totalAmount: {
		type: Number,
		required: [true,"Total amount is required"]
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	},
	associatedWith: [{
		userId:{
			type: String,
			required: [true,"User Id is required"]
		},
		products: [
			{
				productId: {
						type: String,
						required: [true,"Product Id is required"]
				}
			}
		]
	}]
});


module.exports = mongoose.model("Order", orderSchema);