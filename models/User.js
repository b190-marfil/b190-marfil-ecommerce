const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName:{
		type: String,
		required: [true,"First name is required"]
	},
	lastName:{
		type: String,
		required: [true,"Last name is required"]
	},
	email:{
		type: String,
		required: [true,"Email is required"]
	},
	password:{
		type: String,
		required: [true,"Password is required"]
	},
	isAdmin:{
		type: Boolean,
		default: false
	},
	mobileNo:{
		type: String,
		required: [true,"Mobile No. is required"]
	},
	orders: [
		{
			orderId: {
				type: String,
				// required: [true,"Order Id is required"]
			},
			totalAmount: {
				type: Number,
				// required: [true,"Total amount is required"]
			},
			purchasedOn: {
				type: Date,
				default: new Date()
			},
			products: [
				{
					productId: {
						type: String,
						// required: [true,"Product Id is required"]
					}
				}
			]
		}
	]
});

module.exports = mongoose.model("User", userSchema);