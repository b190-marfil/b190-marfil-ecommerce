const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController.js");
const auth = require("../auth.js");




// Create a product
router.post("/create", auth.verify, (req, res) => {
	if(auth.decode(req.headers.authorization).isAdmin === true) {
		productController.addProduct(req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(`Unauthorized command.`);
	}
});

// Retrieve all active products
router.get("/",(req,res)=>{
	productController.getAllActiveProducts().then(resultFromController=>{res.send(resultFromController)});
});

// Retrieve a single product
router.get("/:productId",(req,res) =>{
	productController.getProduct(req.params).then(resultFromController=>{res.send(resultFromController)});
});

// Update Product information
router.put("/:productId",auth.verify,(req,res)=>{
	if(auth.decode(req.headers.authorization).isAdmin === true) {
		productController.updateProduct(req.params,req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(`Unauthorized command.`);
	}
});

// Archive product
router.put("/:productId/archive",auth.verify,(req,res)=>{
	if(auth.decode(req.headers.authorization).isAdmin === true) {
		productController.archiveProduct(req.params).then(resultFromController=>{res.send(resultFromController)});
	} else {
		res.send(`Unauthorized command.`);
	}
});


module.exports = router;	