const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderController.js");
const auth = require("../auth.js");




// Create order
router.post("/create",auth.verify,(req,res)=>{
	let data = {
		userId : auth.decode(req.headers.authorization).id,
		reqBody : req.body
	}
	if(auth.decode(req.headers.authorization).isAdmin === false) {
		orderController.createOrder(data).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(`Unauthorized command.`);
	}
});

// Retrieve all orders (Admin only)
router.get("/",auth.verify,(req,res)=>{
	if(auth.decode(req.headers.authorization).isAdmin === true) {
		orderController.getAllOrders().then(resultFromController => res.send(resultFromController));
	} else {
		res.send(`Unauthorized command.`);
	}
});

module.exports = router;	
