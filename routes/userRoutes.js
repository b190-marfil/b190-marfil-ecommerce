const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController.js")
const auth = require("../auth.js");

// Get the list of all users
router.get("/",(req,res)=>{
	userController.getAllUsers().then(resultFromController => res.send(resultFromController));
});

// User Registration
router.post("/register",(req,res)=>{
	userController.checkEmailExists(req.body).then(doesEmailExists=>{
		userController.registerUser(req.body,doesEmailExists).then(result => res.send(result))
	})
});

// User Login
router.post("/login",(req,res)=>{
	userController.loginUser(req.body).then(resultFromController=>res.send(resultFromController));
});

// Set as Admin
router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {
	if(auth.decode(req.headers.authorization).isAdmin === false) {
		res.send(`Unauthorized command.`);
	} else {
		userController.setAsAdmin(req.params.userId).then(resultFromController => res.send(resultFromController));
	}
});

// Retrieve authenticated user's orders (Non-admin only)
router.get("/myOrders",auth.verify,(req,res)=>{
	let userId = auth.decode(req.headers.authorization).id;
	if(auth.decode(req.headers.authorization).isAdmin === true) {
		res.send(`Unauthorized command.`);
	} else {
		userController.getUserOrders(userId).then(resultFromController => res.send(resultFromController));
	}
});

module.exports = router;
