const Order = require("../models/Order.js");
const Product = require("../models/Product.js");
const User = require("../models/User.js");
const auth = require("../auth.js");



// Create order
module.exports.createOrder = async(data) =>{
   	let totalPrice = 0;
   	let productIds = [];
   	let newOrderId;
   	
   	let addedProducts = await Product.find({"_id": {$in: data.reqBody.productId}});

   	for(i = 0; i < addedProducts.length; i++){
   		totalPrice += addedProducts[i].price;
   		productIds.push({productId: addedProducts[i].id})
   	};

   	let newOrder = new Order({
   		totalAmount: totalPrice,
   		associatedWith: [{
   			userId: data.userId,
   			products: productIds
   		}]
   	});

   	return newOrder.save().then(savedOrder => {
		newOrderId = savedOrder.id;
   		return User.findById(data.userId).then(user => {
			user.orders.push({ orderId : newOrderId , totalAmount : totalPrice , products : productIds });
   			return user.save().then((savedData, error)=>{
				if(error){
					return false;
				}else{
					return savedData;
				}
			})
		});
   	});
};

// Retrieve all orders (Admin only)
module.exports.getAllOrders = ()=>{
	return Order.find().then(allOrders => {
		return allOrders;
	});
}

// Retrieve all orders (Admin only)
