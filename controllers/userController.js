const User = require("../models/User.js");
const Product = require("../models/Product.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

// Get the list of all users
module.exports.getAllUsers=()=>{
	return User.find({}).then(result=>{
		return result;
	});
};

// Check if email exists
module.exports.checkEmailExists = (reqBody)=>{
	return User.find({email:reqBody.email}).then(result =>{
		if(result.length>0){
			return true
		}else{
			return false
		}
	})
};

// User Registration
module.exports.registerUser = (reqBody,doesEmailExists)=>{
	if(doesEmailExists === false){
		let newUser = new User({
			firstName: reqBody.firstName,
			lastName: reqBody.lastName,
			email: reqBody.email,
			mobileNo: reqBody.mobileNo,
			password: bcrypt.hashSync(reqBody.password, 10)
		})
		return newUser.save().then((user,error)=>{
			if(error){
				return error;
			}else{
				return `User successfully registered.`;
			}
		})
	}else{
		return Promise.resolve('Email is already in use.').then(message=>{
			return message
		});
	}
};

// User Login
module.exports.loginUser = ( reqBody ) => {
	return User.findOne( { email: reqBody.email } ).then(result =>{
		if(result === null){
			return `User not found.`;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return { access: auth.createAccessToken(result) }
			}else{
				return `Incorrect password. Please try again.`;
			}
		}
	} )
};

// Set as Admin
module.exports.setAsAdmin = ( userId ) => {
	return User.findById(userId).then((userData, err)=>{
		if (err) {
			return err;
		}else{
			if(userData.isAdmin === true){
				return `${userData.firstName} is already an admin.`;
			}else if(userData.isAdmin === false){
				userData.isAdmin = true;
				return userData.save().then((updatedUser,err)=>{
					if(err){
						return err;
					}else{
						return `${userData.firstName} is now an admin.`;
					}	
				});
			}
		}
	});
};

// Retrieve authenticated user's orders (Non-admin only)
module.exports.getUserOrders = (userId)=>{
	return User.findById(userId).then(user => {
		if(user.orders.length > 0){
			return user.orders;
		}else{
			return `No orders found.`
		}
	})
};