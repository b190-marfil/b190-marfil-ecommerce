const Product = require("../models/Product.js");
const auth = require("../auth.js");






// Create a product
module.exports.addProduct = (data) => {
		let newProduct = new Product({
			name : data.name,
			description : data.description,
			price : data.price
		});
		return newProduct.save().then((product, error) => {
			if (error) {
				return error;
			} else {
				return `Product has been created successfully.`;
			};
		});
};

// Retrieve a single product
module.exports.getProduct = (reqParams) =>{
	return Product.findById(reqParams.productId).then(result=>{
		return result;
	})
};

// Retrieve all active products
module.exports.getAllActiveProducts = () =>{
	return Product.find({isActive: true}).then(result=>{
		return result;
	})
};

// Update Product information
module.exports.updateProduct = (reqParams,reqBody)=>{
	let updateProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	return Product.findByIdAndUpdate(reqParams.productId,updateProduct).then((product,err)=>{
		if(err){
			return err;
		}else{
			return `Product has been updated successfully.`;
		}
	})
};

// Archive product
module.exports.archiveProduct = (reqParams)=>{
	let updateActiveField = {
		isActive: false
	};
	return Product.findByIdAndUpdate(reqParams.productId,updateActiveField).then((course,err)=>{
		if(err){
			return err;
		}else{
			return `Product has been archived successfully.`;
		}
	})
};