const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./routes/userRoutes.js");
const productRoutes = require("./routes/productRoutes.js");
const orderRoutes = require("./routes/orderRoutes.js");


const app = express();

// MongoDB Connection
mongoose.connect("mongodb+srv://zooomxc:admin@wdc028-course-booking.igawzgs.mongodb.net/b190-ecommerce?retryWrites=true&w=majority",
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);


let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", ()=> console.log("We're connected to the database"));


app.use(cors());

app.use(express.json());
app.use(express.urlencoded({extended:true}))

app.use("/users",userRoutes);
app.use("/products",productRoutes);
app.use("/orders",orderRoutes);



app.listen(process.env.PORT || 3000, ()=>{console.log(`API now online at port ${process.env.PORT || 3000}`)});